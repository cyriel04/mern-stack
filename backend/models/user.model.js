const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema(
	{
		username: {
			type: String,
			required: true,
			unique: true,
			trim: true,
			minlength: 2
		}
	},
	{ timestamps: true }
);
mongoose.models = {};
const User = mongoose.model("User", userSchema);

module.exports = User;
