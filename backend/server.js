const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

const dotenv = require("dotenv").config;

const app = express();

// mongoose.Promise = global.Promise;

// Connect MongoDB at default port 27017.
// const uri = process.env.ATLAS_URI;
// mongoose.connect("127.0.0.1:27017", {
// 	useNewUrlParser: true,
// 	useCreateIndex: true
// });

app.use(cors());
//Body parser
app.use(express.json());

//Urlencoded
// app.use(express.urlencoded({ extended: false }));

//Connect to local DB
mongoose.connect("mongodb://localhost:27017/mern", {
	useNewUrlParser: true,
	useCreateIndex: true,
	useUnifiedTopology: true
});

const connection = mongoose.connection;
connection.once("open", () => {
	console.log("Mongoose running");
});

//Main Route
const usersRouter = require("./routes/users");
const exercisesRouter = require("./routes/exercises");
app.use("/users", usersRouter);
app.use("/exercises", exercisesRouter);

//Port start
const PORT = process.env.PORT || 7000;
app.listen(PORT, () => console.log(`${PORT} running`));
